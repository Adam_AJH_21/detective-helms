﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading;
using UnityEngine.SceneManagement;

public class Countdown : MonoBehaviour
{
    public int timeLeft = 30;
    public Text countdown;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("LoseTime");
        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        countdown.text = ("Time: " + timeLeft);

        if (timeLeft <= 0)
            GameOver();
    }
    //Displays the time left to the player theough text, if the time hits 0, activates 'Game Over' scene

    private void GameOver()
    {
        SceneManager.LoadScene("Game Over");
    }
    //Sends the player to the Game Over screen

    IEnumerator LoseTime()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            timeLeft--;
        }
    }
}
