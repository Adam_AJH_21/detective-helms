﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{

    public float horizontalInput;
    public float forwardInput;
    public float speed = 5.0f;
    public bool resetScore = false;
    public AudioSource pickupSound;

    public static int count;
    public Text countText;

    public Animator animator;
    
    // Start is called before the first frame update
    void Start()
    {
        if (resetScore == true)
        {
            count = 0;
        }
        //When the game is started, the previous score will be reset for the new game

        SetCountText();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 input= Vector3.zero;

        input.x = Input.GetAxis("Horizontal");
        transform.Translate(Vector3.right * input.x * Time.deltaTime * speed, Space.World);

        input.z = Input.GetAxis("Vertical");
        transform.Translate(Vector3.forward * input.z * Time.deltaTime * speed, Space.World);
        //Character movement, left/right and forward/backward

        animator.SetFloat("MoveSpeed", input.magnitude);

        Vector3 newPosition = new Vector3(input.x, 0.0f, input.z);
        transform.LookAt(newPosition + transform.position);
        //Character will look in the direction they are moving

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
            pickupSound.Play();
        }
        //Character can 'pick up' gem objects, the score will increase by 1 and a sound will play upon pickup
    }
    void SetCountText()
    {
        countText.text = "Gems: " + count.ToString();
    }
    //Display the Score with score text
}